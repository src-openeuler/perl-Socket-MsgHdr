Name:           perl-Socket-MsgHdr
Version:        0.05
Release:        6
Summary:        Sendmsg, recvmsg and ancillary data operations
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Socket-MsgHdr
Source0:        https://cpan.metacpan.org/authors/id/F/FE/FELIPE/Socket-MsgHdr-%{version}.tar.gz

BuildRequires:  findutils make gcc perl(ExtUtils::MakeMaker) perl-interpreter
BuildRequires:  perl-devel perl-generators perl(Test::More) perl(Exporter)
BuildRequires:  perl(Socket) perl(strict) perl(XSLoader)

%description
Advanced socket messaging operations: sendmsg, recvmsg and cmsghdr manipulation. Available as
plain functions or IO::Socket methods.

XS handles structure packing (possible but unpleasant in Perl) and wraps the syscalls; you and
Perl do the rest!

%package_help

%prep
%autosetup -n Socket-MsgHdr-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Socket*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.05-6
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Mar 5 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.05-5
- Package init
